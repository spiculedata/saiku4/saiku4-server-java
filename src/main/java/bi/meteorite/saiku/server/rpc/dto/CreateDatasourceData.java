package bi.meteorite.saiku.server.rpc.dto;

import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.datasources.impl.Mondrian4Datasource;
import bi.meteorite.saiku.server.mondrian4.connections.ISaikuConnection;
import lombok.Data;
import org.saiku.web.rest.objects.DataSourceMapper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

@Data
public class CreateDatasourceData {
  private String enabled;
  private String connectionname;
  private String jdbcurl;
  private String schema;
  private String driver;
  private String username;
  private String password;
  private String connectiontype;
  private String id;
  private String path;
  private String advanced;
  private String security_type;
  private String propertyKey;
  private String csv;
  private String schema_data;

  public ISaikuDatasource toISaikuDatasource() {
    Properties props = new Properties();

    if (connectionname != null) props.setProperty(ISaikuConnection.NAME_KEY, connectionname);
    props.setProperty(ISaikuConnection.DRIVER_KEY, "mondrian.olap4j.MondrianOlap4jDriver");
    if (jdbcurl != null) props.setProperty(ISaikuConnection.URL_KEY, jdbcurl);
    if (username != null) props.setProperty(ISaikuConnection.USERNAME_KEY, username);
    if (password != null) props.setProperty(ISaikuConnection.PASSWORD_KEY, password);
    if (csv != null) props.setProperty("csv", csv);
    if (enabled != null) props.setProperty("enabled", enabled);
    if (schema != null) props.setProperty("schema", schema);
    if (connectiontype != null) props.setProperty("connectiontype", connectiontype);
    if (id != null) props.setProperty("id", id);
    if (advanced != null) props.setProperty("advanced", advanced);
    if (security_type != null) props.setProperty("security_type", security_type);
    if (propertyKey != null) props.setProperty("propertyKey", propertyKey);


    try {
      File schemaFile = File.createTempFile("schema_" + schema, ".xml");
      PrintWriter writer = new PrintWriter(new FileWriter(schemaFile));
      writer.print(schema_data);
      writer.close();

      props.setProperty("schema_path", schemaFile.getPath());

      props.setProperty("location", "jdbc:mondrian:Jdbc=" + jdbcurl + ";" +
          "Catalog=file://" + schemaFile.getPath() + ";JdbcDrivers=" + driver);
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }

    return new Mondrian4Datasource(connectionname, props);
  }
}
