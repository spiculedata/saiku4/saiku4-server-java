package bi.meteorite.saiku.server.rpc;

import lombok.Data;

@Data
public class SaikuRequest {
  private String licenseKey;
  private String name;
  private String company;
  private String jsonPayload;
}
