package bi.meteorite.saiku.server.rpc.consumers;

import java.io.File;
import java.io.FileInputStream;


import bi.meteorite.saiku.server.reporting.ReportServer;
import bi.meteorite.saiku.server.reporting.util.MockUriInfo;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenerateReportConsumer extends SaikuDefaultConsumer {
  private static ReportServer server;

  @Autowired
  public GenerateReportConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);

    if (server == null) {
      server = new ReportServer();
      server.init();
    }
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    File temp = File.createTempFile("saiku", ".pdf");
    server.processReport(temp, "test", "pdf", new MockUriInfo());

    byte[] buffer = new byte[(int) temp.length()];

    try {
      FileInputStream fis = new FileInputStream(temp);
      fis.read(buffer);
      fis.close();
    } catch (Exception ex) {
      System.err.println("Error converting the report to PDF: " + ex.getMessage());
    }

    return buffer;
  }
}
