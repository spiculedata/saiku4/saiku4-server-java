package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EditDatasourceConsumer extends SaikuDefaultConsumer {
  @Autowired
  public EditDatasourceConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    return "CreateDatasourceConsumer".getBytes();
  }
}
