package bi.meteorite.saiku.server.rpc.dto;

import lombok.Data;

@Data
public class ExportImageDto {
  private String imageType;
  private String svg;
  private String size;
}
