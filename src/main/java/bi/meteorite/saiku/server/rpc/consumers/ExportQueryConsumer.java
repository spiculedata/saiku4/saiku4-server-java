package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.licensing.annotations.LicenseRequired;
import bi.meteorite.saiku.server.query.SaikuQueryService;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import bi.meteorite.saiku.server.rpc.dto.ExportQueryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExportQueryConsumer extends SaikuDefaultConsumer {
  private static final Logger logger = LoggerFactory.getLogger(ExportQueryConsumer.class);

  @Autowired
  private SaikuQueryService saikuQueryService;

  public ExportQueryConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  @LicenseRequired
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    ExportQueryDto data = parse(request.getJsonPayload(), ExportQueryDto.class);
    byte[] result = new byte[0];

    try {
      if (data.getOutputFormat().equals("PDF")) {
        result = saikuQueryService.exportPdf(data.getQueryName(), data.getFormat());
      } else if (data.getOutputFormat().equals("CSV")) {
        result = saikuQueryService.exportCsv(data.getQueryName(), data.getFormat());
      } else if (data.getOutputFormat().equals("XLS")) {
        result = saikuQueryService.exportXls(data.getQueryName(), data.getFormat());
      } else {
        throw new Exception("Unknown output file format: " + data.getOutputFormat());
      }
    } catch (Exception ex) {
      logger.error("Query Export Error", ex);
    }

    return result;
  }
}
