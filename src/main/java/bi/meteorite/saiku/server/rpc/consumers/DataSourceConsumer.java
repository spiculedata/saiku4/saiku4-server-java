package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.datasources.Datasources;
import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.mondrian4.connections.IConnectionManager;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataSourceConsumer extends SaikuDefaultConsumer {
  @Autowired
  public DataSourceConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Autowired
  private Datasources datasources;

  @Autowired
  private IDatasourceManager ids;

  @Autowired
  private IConnectionManager icm;


  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    return "Datasources Consumed".getBytes();
  }
}
