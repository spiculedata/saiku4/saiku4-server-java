package bi.meteorite.saiku.server.rpc.producer;

import bi.meteorite.saiku.server.rpc.Protocol;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class DatasourceProducer extends SaikuDefaultProducer {
    private String routingKey;

    public DatasourceProducer(Protocol rpcProtocol) throws IOException {
        super(rpcProtocol);
    }

    @Override
    public void setRoutingKey() {
        this.routingKey = "mondrianrequests";
    }

    @Override
    public String getRoutingKey() {
        return routingKey;
    }

    @Override
    public byte[] publishMessage(byte[] msg) {
        return msg;
    }

}
