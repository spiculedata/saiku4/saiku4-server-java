package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import bi.meteorite.saiku.server.rpc.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class QueryEndpointsConsumer extends SaikuDefaultConsumer {
  @Autowired
  public QueryEndpointsConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    List<String> endpoints = new ArrayList<>();

    endpoints.add("email_password_auth");
    endpoints.add("cookie_auth");
    endpoints.add("cas_header_auth");
    endpoints.add("generate_report");
    endpoints.add("create_datasource");
    endpoints.add("list_datasources");
    endpoints.add("discover_datasources");
    endpoints.add("get_cube_metadata");
    endpoints.add("edit_datasource");
    endpoints.add("delete_datasource");
    endpoints.add("datasource");
    endpoints.add("execute_query");
    endpoints.add("validate_license_key");
    endpoints.add("query_endpoints");

    return Util.listToJson(endpoints).getBytes();
  }
}
