package bi.meteorite.saiku.server.rpc.dto;

import lombok.Data;

@Data
public class DiscoverCubeMetadataDto {
  private String username;
  private String connection;
  private String catalog;
  private String schema;
  private String cube;
}
