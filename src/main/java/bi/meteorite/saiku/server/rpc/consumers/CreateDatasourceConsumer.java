package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import bi.meteorite.saiku.server.rpc.dto.CreateDatasourceData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;

@Component
public class CreateDatasourceConsumer extends SaikuDefaultConsumer {
  @Autowired
  private IDatasourceManager dsm;

  @Autowired
  public CreateDatasourceConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    CreateDatasourceData datasourceData = parse(request.getJsonPayload(), CreateDatasourceData.class);

    ISaikuDatasource datasource = datasourceData.toISaikuDatasource();

    // Adding the schema
    dsm.addSchema(datasourceData.getSchema_data(),
        datasource.getProperties().getProperty("schema_path"),
        datasourceData.getSchema());

    // Adding the datasource
    dsm.addDatasource(datasource);

    return "ok".getBytes();
  }
}
