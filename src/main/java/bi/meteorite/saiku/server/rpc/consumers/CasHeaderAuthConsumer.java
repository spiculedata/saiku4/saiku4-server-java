package bi.meteorite.saiku.server.rpc.consumers;

import java.util.Map;

import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import bi.meteorite.saiku.server.rpc.Util;
import bi.meteorite.saiku.server.session.SaikuSession;
import bi.meteorite.saiku.server.session.SaikuSessionManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static bi.meteorite.saiku.server.rpc.Util.buildMap;
import static bi.meteorite.saiku.server.rpc.Util.entry;

@Component
public class CasHeaderAuthConsumer extends SaikuDefaultConsumer {
  @Autowired
  public CasHeaderAuthConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    Map<String, Object> fields = Util.jsonToMap(request.getJsonPayload());

    SaikuSession session = SaikuSessionManager.getInstance().createSessionWithEmailAndPassword(
        (String) fields.get("cas_header"),
        (String) fields.get("cas_header"));

    return Util.mapToJson(buildMap(entry("sessionId", session.getSessionId()))).getBytes();
  }
}
