package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DiscoverDatasourcesConsumer extends SaikuDefaultConsumer {
  private static final Logger logger = LoggerFactory.getLogger(DiscoverDatasourcesConsumer.class);

  @Autowired
  private IDatasourceManager dsm;

  @Autowired
  public DiscoverDatasourcesConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    List<org.saiku.olap.dto.SaikuConnection> oldSaikuConnections = dsm.getSaikuConnections();
    return toJsonBytes(oldSaikuConnections);
  }
}
