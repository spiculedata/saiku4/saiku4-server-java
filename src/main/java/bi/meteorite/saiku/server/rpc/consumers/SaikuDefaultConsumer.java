package bi.meteorite.saiku.server.rpc.consumers;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import bi.meteorite.saiku.server.licensing.*;
import org.json.JSONObject;

import bi.meteorite.saiku.server.licensing.annotations.LicenseRequired;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import bi.meteorite.saiku.server.rpc.Util;
import bi.meteorite.saiku.server.rpc.producer.DatasourceProducer;
import bi.meteorite.saiku.server.rpc.producer.LicensingProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import bi.meteorite.saiku.server.rpc.Protocol;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class SaikuDefaultConsumer extends DefaultConsumer {
  public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

  private Protocol rpcProtocol;
  @Autowired
  private LicensingService licensingService;

  @Autowired
  private LicensingProducer dsp;

  @Autowired
  public SaikuDefaultConsumer(Protocol rpcProtocol) {
    super(rpcProtocol.getChannel());
    this.rpcProtocol = rpcProtocol;
  }

  public Protocol getRpcProtocol() {
    return rpcProtocol;
  }

  public final byte[] processMessage(byte[] body) throws Exception {
    String jsonData = new String(body, DEFAULT_CHARSET);
    SaikuRequest request = parse(jsonData, SaikuRequest.class);

    // If the subclass 'processSaikuRequest' is annotated, we check the user's license status
    Method processSaikuRequestMethod = this.getClass().getMethod("processSaikuRequest", SaikuRequest.class);
    if (processSaikuRequestMethod.isAnnotationPresent(LicenseRequired.class)) {
      LicenseRequired licenseRequired = processSaikuRequestMethod.getAnnotation(LicenseRequired.class);

      System.out.println("Checking license validity: "+request.getLicenseKey()+" "+request.getName()+ " "+ request.getCompany());
      if (StringUtils.isEmpty(request.getLicenseKey()) || StringUtils.isBlank(request.getLicenseKey())) {
        throw new LicenseException("You don't have a license key");
      }

      System.out.println("Constructing validator");
      SaikuLicense4J2 lic = new SaikuLicense4J2();
      lic.setCompany(request.getCompany());
      lic.setName(request.getName());
      lic.setKey(request.getLicenseKey());
      lic.setOperation("validate");
      JSONObject jsonObj = new JSONObject( lic );
      SaikuRequest req = new SaikuRequest();
      req.setJsonPayload(jsonObj.toString());

      JSONObject r = new JSONObject(req);
      System.out.println("Sending request:"+r.toString());


      String response = dsp.handleDeliery(r.toString().getBytes());

      System.out.println(response);
      ObjectMapper mapper = new ObjectMapper();
      LicensingStatus resp = mapper.readValue(response, LicensingStatus.class);
      if (!resp.isValid()) {
        System.out.println("Invalid license");

        throw new LicenseException("Your license key is not valid");
        //return processSaikuRequest(request);
      }

      if (licenseRequired.itShouldBeActivated() && !resp.isActivated()) {
        //throw new LicenseException("Your license key has not been activated yet");
        System.out.println("Not activated");
        return processSaikuRequest(request);
      }
    }

    return processSaikuRequest(request);
  }

  protected <T> T parse(String json, Class<T> valueType) throws IOException {
    return createObjectMapper().readValue(json, valueType);
  }

  protected String toJsonString(Object obj) throws JsonProcessingException {
    return createObjectMapper().writeValueAsString(obj);
  }

  protected byte[] toJsonBytes(Object obj) throws JsonProcessingException {
    return toJsonString(obj).getBytes(DEFAULT_CHARSET);
  }

  protected ObjectMapper createObjectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return objectMapper;
  }

  public abstract byte[] processSaikuRequest(SaikuRequest request) throws Exception;

  @Override
  public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
      throws IOException {
    AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();

    try {
      this.getChannel().basicPublish("", properties.getReplyTo(), replyProps, this.processMessage(body));
    } catch (Exception ex) {
      System.err.println("Error sending the report bytes through queue: " + ex.getMessage());
      this.getChannel().basicPublish("", properties.getReplyTo(), replyProps, this.createExceptionMessage(ex));
    }

    this.getChannel().basicAck(envelope.getDeliveryTag(), false);

    // RabbitMq consumer worker thread notifies the RPC server owner thread
    synchronized (this) {
      this.notify();
    }
  }

  private byte[] createExceptionMessage(Exception ex) {
    Map<String, Object> map = new HashMap<>();

    StringWriter errorOutput = new StringWriter();
    ex.printStackTrace(new PrintWriter(errorOutput));

    map.put("status", "error");
    map.put("message", ex.getMessage());
    map.put("stackTrace", errorOutput.toString());

    return Util.mapToJson(map).getBytes(DEFAULT_CHARSET);
  }
}
