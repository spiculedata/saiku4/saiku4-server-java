package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.query.SaikuQueryService;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import bi.meteorite.saiku.server.rpc.dto.DiscoverCubeMetadataDto;
import org.apache.commons.lang3.StringUtils;
import org.olap4j.OlapConnection;
import org.olap4j.OlapException;
import org.olap4j.metadata.*;
import org.saiku.olap.dto.SaikuCube;
import org.saiku.olap.dto.SaikuCubeMetadata;
import org.saiku.olap.dto.SaikuDimension;
import org.saiku.olap.dto.SaikuMember;
import org.saiku.olap.util.ObjectUtil;
import org.saiku.olap.util.SaikuDimensionCaptionComparator;
import org.saiku.olap.util.exception.SaikuOlapException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class DiscoverCubeMetadataConsumer extends SaikuDefaultConsumer {
  private static final Logger logger = LoggerFactory.getLogger(DiscoverCubeMetadataConsumer.class);

  @Autowired
  private IDatasourceManager dsm;
  @Autowired
  private SaikuQueryService saikuQueryService;

  @Autowired
  public DiscoverCubeMetadataConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    DiscoverCubeMetadataDto data = parse(request.getJsonPayload(), DiscoverCubeMetadataDto.class);

    SaikuCube cube = new SaikuCube(data.getConnection(), data.getCube(), data.getCube(), data.getCube(),
        data.getCatalog(), data.getSchema());

    SaikuCubeMetadata result = new SaikuCubeMetadata(null, null, null);

    try {
      List<SaikuDimension> dimensions = getAllDimensions(cube);
      List<SaikuMember> measures = getAllMeasures(cube);
      Map<String, Object> properties = saikuQueryService.getProperties(cube);
      result = new SaikuCubeMetadata(dimensions, measures, properties);
    } catch (Exception e) {
      logger.error(this.getClass().getName(), e);
    }

    return toJsonBytes(result);
  }

  private List<SaikuDimension> getAllDimensions(SaikuCube cube) throws SaikuOlapException {
    Cube nativeCube = getNativeCube(cube);
    List<SaikuDimension> dimensions = ObjectUtil.convertDimensions(nativeCube.getDimensions());
    for (int i=0; i < dimensions.size();i++) {
      SaikuDimension dim = dimensions.get(i);
      if (dim.getName().equals("Measures") || dim.getUniqueName().equals("[Measures]")) {
        dimensions.remove(i);
        break;
      }
    }
    Collections.sort(dimensions, new SaikuDimensionCaptionComparator());
    return dimensions;
  }

  public Cube getNativeCube(SaikuCube cube) throws SaikuOlapException {
    try {
      OlapConnection con = dsm.getOlapConnection(cube.getConnection());
      if (con != null ) {
        for (Database db : con.getOlapDatabases()) {
          Catalog cat = db.getCatalogs().get(cube.getCatalog());
          if (cat != null) {
            for (Schema schema : cat.getSchemas()) {
              if ((StringUtils.isBlank(cube.getSchema()) && StringUtils.isBlank(schema.getName())) || schema.getName().equals(cube.getSchema())) {
                for (Cube cub : schema.getCubes()) {
                  if (cub.getName().equals(cube.getName()) || cub.getUniqueName().equals(cube.getName())) {
                    return cub;
                  }
                }
              }
            }
          }
        }
      }
    } catch (Exception e) {
      throw new SaikuOlapException("Cannot get native cube for ( " + cube+ " )",e);
    }

    throw new SaikuOlapException("Cannot get native cube for ( " + cube+ " )");
  }

  private List<SaikuMember> getAllMeasures(SaikuCube cube) throws SaikuOlapException {
    List<SaikuMember> measures = new ArrayList<>();
    try {
      Cube nativeCube = getNativeCube(cube);
      for (Measure measure : nativeCube.getMeasures()) {
        if(measure.isVisible()) {
          measures.add(ObjectUtil.convertMeasure(measure));
        }
      }
      if (measures.size() == 0) {
        Hierarchy hierarchy = nativeCube.getDimensions().get("Measures").getDefaultHierarchy();
        measures = (ObjectUtil.convertMembers(hierarchy.getRootMembers()));
      }
    } catch (OlapException e) {
      throw new SaikuOlapException("Cannot get measures for cube:"+cube.getName(),e);
    }

    return measures;
  }
}
