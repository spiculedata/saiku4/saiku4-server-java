package bi.meteorite.saiku.server.rpc.producer;

import bi.meteorite.saiku.server.rpc.Protocol;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LicensingProducer extends SaikuDefaultProducer {
    private String routingKey;

    public LicensingProducer(Protocol rpcProtocol) throws IOException {
        super(rpcProtocol);
    }

    @Override
    public void setRoutingKey() {
        this.routingKey = "validate_license_key";
    }

    @Override
    public String getRoutingKey() {
        return routingKey;
    }

    @Override
    public byte[] publishMessage(byte[] msg) {
        return msg;
    }

}
