package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.licensing.annotations.LicenseRequired;
import bi.meteorite.saiku.server.query.SaikuQueryService;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import bi.meteorite.saiku.server.rpc.dto.ExportImageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExportImageConsumer extends SaikuDefaultConsumer {
  private static final Logger logger = LoggerFactory.getLogger(ExportImageConsumer.class);

  @Autowired
  private SaikuQueryService saikuQueryService;

  public ExportImageConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  @LicenseRequired
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    ExportImageDto data = parse(request.getJsonPayload(), ExportImageDto.class);
    byte[] result = new byte[0];

    try {
      Integer size = null;

      try {
        size = new Integer(data.getSize());
      } catch (Exception ignoreItIfSizeNotValidException) {}

      result = saikuQueryService.exportImage(data.getImageType(), data.getSvg(), size);
    } catch (Exception ex) {
      logger.error("Image Export Error", ex);
    }

    return result;
  }
}
