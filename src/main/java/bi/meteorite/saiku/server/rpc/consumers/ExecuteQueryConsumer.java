package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.licensing.annotations.LicenseRequired;
import bi.meteorite.saiku.server.query.SaikuQueryService;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import org.olap4j.CellSet;
import org.saiku.olap.dto.resultset.CellDataSet;
import org.saiku.olap.query2.ThinQuery;
import org.saiku.olap.util.OlapResultSetUtil;
import org.saiku.olap.util.formatter.CellSetFormatterFactory;
import org.saiku.olap.util.formatter.ICellSetFormatter;
import org.saiku.web.rest.objects.resultset.QueryResult;
import org.saiku.web.rest.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExecuteQueryConsumer extends SaikuDefaultConsumer {
  private static final Logger logger = LoggerFactory.getLogger(ExecuteQueryConsumer.class);

  @Autowired
  private SaikuQueryService saikuQueryService;

  public ExecuteQueryConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  @LicenseRequired
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    ThinQuery thinQuery = parse(request.getJsonPayload(), ThinQuery.class);
    String responseJson = "{}";

    try {
      responseJson = toJsonString(saikuQueryService.execute(thinQuery));
    } catch (Exception ex) {
      logger.error("Query Execution Error", ex);
    }

    return responseJson.getBytes(DEFAULT_CHARSET);
  }
}
