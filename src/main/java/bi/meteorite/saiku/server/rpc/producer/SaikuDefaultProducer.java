package bi.meteorite.saiku.server.rpc.producer;

import bi.meteorite.saiku.server.rpc.Protocol;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public abstract class SaikuDefaultProducer implements AutoCloseable {
    private Channel channel;

    @Autowired
    public SaikuDefaultProducer(Protocol rpcProtocol) throws IOException {
        this.setRoutingKey();
        channel = rpcProtocol.getConnection().createChannel();
        rpcProtocol.getChannel().queueDeclare(this.getRoutingKey(), false, false, false, null);
    }

    abstract public void setRoutingKey();
    abstract public String getRoutingKey();

    public abstract byte[] publishMessage(byte[] msg);

    public String handleDeliery(byte[] body) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        String replyQueueName = channel.queueDeclare().getQueue();

        AMQP.BasicProperties properties = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", this.getRoutingKey(),properties, this.publishMessage(body));
        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        channel.basicCancel(ctag);
        return result;
    }
    public void close() throws IOException {
        this.channel.getConnection().close();
    }

}
