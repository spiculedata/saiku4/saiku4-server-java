package bi.meteorite.saiku.server.rpc.dto;

import lombok.Data;

@Data
public class ExportQueryDto {
  private String queryName;
  private String format;
  private String outputFormat;
}
