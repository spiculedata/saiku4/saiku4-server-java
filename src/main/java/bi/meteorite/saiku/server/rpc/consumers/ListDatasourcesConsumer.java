package bi.meteorite.saiku.server.rpc.consumers;

import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.mondrian4.connections.ISaikuConnection;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.SaikuRequest;
import com.google.common.collect.ImmutableMap;
import org.saiku.web.rest.objects.DataSourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ListDatasourcesConsumer extends SaikuDefaultConsumer {
  @Autowired
  private IDatasourceManager dsm;

  @Autowired
  public ListDatasourcesConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processSaikuRequest(SaikuRequest request) throws Exception {
    ImmutableMap<String, ISaikuDatasource> datasources = dsm.getDatasources(new String[]{});
    List<DataSourceMapper> datasourcesList = new ArrayList<>();

    for (ISaikuDatasource ds : datasources.values()) {
      DataSourceMapper mapper = new DataSourceMapper();

      mapper.setConnectionname(ds.getProperties().getProperty(ISaikuConnection.NAME_KEY));
      mapper.setDriver(ds.getProperties().getProperty(ISaikuConnection.DRIVER_KEY));
      mapper.setJdbcurl(ds.getProperties().getProperty(ISaikuConnection.URL_KEY));
      mapper.setUsername(ds.getProperties().getProperty(ISaikuConnection.USERNAME_KEY));
      mapper.setPassword(ds.getProperties().getProperty(ISaikuConnection.PASSWORD_KEY));
      mapper.setCsv(ds.getProperties().getProperty("csv"));
      mapper.setEnabled(ds.getProperties().getProperty("enabled"));
      mapper.setSchema(ds.getProperties().getProperty("schema"));
      mapper.setConnectiontype(ds.getProperties().getProperty("connectiontype"));
      mapper.setId(ds.getProperties().getProperty("id"));
      mapper.setAdvanced(ds.getProperties().getProperty("advanced"));
      mapper.setSecurity_type(ds.getProperties().getProperty("security_type"));
      mapper.setPropertyKey(ds.getProperties().getProperty("propertyKey"));

      datasourcesList.add(mapper);
    }

    return toJsonBytes(datasourcesList);
  }
}
