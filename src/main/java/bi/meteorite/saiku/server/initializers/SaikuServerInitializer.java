package bi.meteorite.saiku.server.initializers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class SaikuServerInitializer implements CommandLineRunner {
  private static final Logger logger = LoggerFactory.getLogger(SaikuServerInitializer.class);

  @Override
  public void run(String... args) throws Exception {
    logger.info("*** Saiku 4 Server Initialized ***");
  }
}
