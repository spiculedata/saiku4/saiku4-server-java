package bi.meteorite.saiku.server.initializers;

import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.datasources.impl.Mondrian4Datasource;
import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Properties;

@Component
public class DatabaseInitializer implements InitializingBean {
  private static final Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);

  @Value("${db.url}")
  private String databaseUrl;
  @Value("${db.user}")
  private String databaseUser;
  @Value("${db.password}")
  private String databasePassword;
  @Value("${db.encryptpassword}")
  private String databaseEncryptPassword;

  @Value("${foodmart.url}")
  private String foodmartUrl;
  @Value("${foodmart.user}")
  private String foodmartUser;
  @Value("${foodmart.password}")
  private String foodmartPassword;
  @Value("${foodmart.repo}")
  private String foodmartRepository;
  @Value("${foodmart.schema}")
  private String foodmartSchema;

  @Value("${earthquakes.url}")
  private String earthquakesUrl;
  @Value("${earthquakes.user}")
  private String earthquakesUser;
  @Value("${earthquakes.password}")
  private String earthquakesPassword;
  @Value("${earthquakes.repo}")
  private String earthquakesRepository;
  @Value("${earthquakes.schema}")
  private String earthquakesSchema;


  private JdbcDataSource ds;
  @Autowired
  private IDatasourceManager dsm;
  private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  private void init() throws Exception {
    initDB();
    loadUsers();
    loadFoodmart();
    loadEarthquakes();
  }

  private void initDB() {
    ds = new JdbcDataSource();
    ds.setURL(databaseUrl);
    ds.setUser(databaseUser);
    ds.setPassword(databasePassword);
  }

  private void loadUsers() throws SQLException {
    Connection c = ds.getConnection();

    Statement statement = c.createStatement();
    statement.execute("CREATE TABLE IF NOT EXISTS LOG(time TIMESTAMP AS CURRENT_TIMESTAMP NOT NULL, log CLOB);");

    statement.execute("CREATE TABLE IF NOT EXISTS USERS(user_id INT(11) NOT NULL AUTO_INCREMENT, " +
        "username VARCHAR(45) NOT NULL UNIQUE, password VARCHAR(100) NOT NULL, email VARCHAR(100), " +
        "enabled TINYINT NOT NULL DEFAULT 1, PRIMARY KEY(user_id));");

    statement.execute("CREATE TABLE IF NOT EXISTS USER_ROLES (\n"
        + "  user_role_id INT(11) NOT NULL AUTO_INCREMENT,username VARCHAR(45),\n"
        + "  user_id INT(11) NOT NULL REFERENCES USERS(user_id),\n"
        + "  ROLE VARCHAR(45) NOT NULL,\n"
        + "  PRIMARY KEY (user_role_id));");

    ResultSet result = statement.executeQuery("select count(*) as c from LOG where log = 'insert users'");
    result.next();
    if (result.getInt("c") == 0) {
      dsm.createUser("admin");
      dsm.createUser("smith");
      statement.execute("INSERT INTO users(username,password,email, enabled)\n"
          + "VALUES ('admin','admin', 'test@admin.com',TRUE);" +
          "INSERT INTO users(username,password,enabled)\n"
          + "VALUES ('smith','smith', TRUE);");
      statement.execute(
          "INSERT INTO user_roles (user_id, username, ROLE)\n"
              + "VALUES (1, 'admin', 'ROLE_USER');" +
              "INSERT INTO user_roles (user_id, username, ROLE)\n"
              + "VALUES (1, 'admin', 'ROLE_ADMIN');" +
              "INSERT INTO user_roles (user_id, username, ROLE)\n"
              + "VALUES (2, 'smith', 'ROLE_USER');");

      statement.execute("INSERT INTO LOG(log) VALUES('insert users');");
    }

    if (databaseEncryptPassword.equals("true") && !checkUpdatedEncyption()) {
      logger.debug("Encrypting User Passwords");
      updateForEncryption();
      logger.debug("Finished Encrypting Passwords");
    }
  }

  private boolean checkUpdatedEncyption() throws SQLException{
    Connection c = ds.getConnection();

    Statement statement = c.createStatement();
    ResultSet result = statement.executeQuery("select count(*) as c from LOG where log = 'update passwords'");
    result.next();
    return result.getInt("c") != 0;
  }

  private void updateForEncryption() throws SQLException {
    Connection c = ds.getConnection();

    Statement statement = c.createStatement();
    statement.execute("ALTER TABLE users ALTER COLUMN password VARCHAR(100) DEFAULT NULL");

    ResultSet result = statement.executeQuery("select username, password from users");

    while(result.next()){
      statement = c.createStatement();

      String pword = result.getString("password");
      String hashedPassword = passwordEncoder.encode(pword);
      String sql = "UPDATE users " +
          "SET password = '"+hashedPassword+"' WHERE username = '"+result.getString("username")+"'";
      statement.executeUpdate(sql);
    }
    statement = c.createStatement();

    statement.execute("INSERT INTO LOG(log) VALUES('update passwords');");
  }

  private void loadFoodmart() throws SQLException, IOException {
    String url = foodmartUrl;
    String user = foodmartUser;
    String pword = foodmartPassword;
    String ddlScriptPath = getPath("foodmart_h2.sql");
    String schemaFilePath = getPath("FoodMart4.xml");

    if (url != null) {
      JdbcDataSource ds2 = new JdbcDataSource();
      ds2.setURL(url);
      ds2.setUser(user);
      ds2.setPassword(pword);

      Connection c = ds2.getConnection();
      DatabaseMetaData dbm = c.getMetaData();
      ResultSet tables = dbm.getTables(null, null, "account", null);

      if (!tables.next()) {
        logger.info("Foodmart DB does not exists - creating it ...");
        // Table exists
        Statement statement = c.createStatement();

        statement.execute("RUNSCRIPT FROM '" + ddlScriptPath + "'");

        statement.execute("alter table \"time_by_day\" add column \"date_string\" varchar(30);"
            + "update \"time_by_day\" "
            + "set \"date_string\" = TO_CHAR(\"the_date\", 'yyyy/mm/dd');");
      } else {
        logger.info("Foodmart DB already exists");
      }

      String schema = null;

      try {
        schema = readFile(schemaFilePath, StandardCharsets.UTF_8);
      } catch (IOException e) {
        logger.error("Can't read schema file",e);
      }

      try {
        dsm.addSchema(schema, schemaFilePath, null);
      } catch (Exception e) {
        logger.error("Can't add schema file to repo", e);
      }

      if (getRepositoryDir() != null) {
        url = "jdbc:h2:" + getPath("foodmart") + ";MODE=MySQL";
      }

      Properties p = new Properties();

      p.setProperty("driver", "mondrian.olap4j.MondrianOlap4jDriver");
      p.setProperty("location", "jdbc:mondrian:Jdbc=" + url + ";" +
          "Catalog=file://" + schemaFilePath + ";JdbcDrivers=org.h2.Driver");
      p.setProperty("username", "sa");
      p.setProperty("password", "");
      p.setProperty("id", "4432dd20-fcae-11e3-a3ac-0800200c9a66");

      ISaikuDatasource ds = new Mondrian4Datasource("foodmart", p);

      try {
        dsm.addDatasource(ds);
      } catch (Exception e) {
        logger.error("Can't add data source to repo", e);
      }
    }
  }

  private void loadEarthquakes() throws SQLException, IOException {
    String url = earthquakesUrl;
    String user = earthquakesUser;
    String password = earthquakesPassword;
    String schemaFilePath = getPath("Earthquakes.xml");

    if (url != null) {
      JdbcDataSource dataSource = new JdbcDataSource();
      dataSource.setURL(url);
      dataSource.setUser(user);
      dataSource.setPassword(password);

      Connection c = dataSource.getConnection();
      DatabaseMetaData dbm = c.getMetaData();
      ResultSet tables = dbm.getTables(null, null, "earthquakes", null);
      String schema = null;

      if (!tables.next()) {
        logger.info("Earthquakes DB does not exists - creating it ...");

        Statement statement = c.createStatement();

        statement.execute("RUNSCRIPT FROM '" + getPath("earthquakes.sql") + "'");
        statement.executeQuery("select 1");
      } else {
        logger.info("Earthquakes DB already exists");
      }

      try {
          schema = readFile(schemaFilePath, StandardCharsets.UTF_8);
      } catch (IOException e) {
        logger.error("Can't read schema file", e);
      }

      try {
        dsm.addSchema(schema, schemaFilePath, null);
      } catch (Exception e) {
        logger.error("Can't add schema file to repo", e);
      }

      if (getRepositoryDir() != null) {
        url = "jdbc:h2:" + getPath("earthquakes") + ";MODE=MySQL";
      }

      Properties p = new Properties();

      p.setProperty("advanced", "true");
      p.setProperty("driver", "mondrian.olap4j.MondrianOlap4jDriver");
      p.setProperty("location",
          "jdbc:mondrian:Jdbc=" + url + ";" +
              "Catalog=file://" + schemaFilePath + ";JdbcDrivers=org.h2.Driver");
      p.setProperty("username", "sa");
      p.setProperty("password", "");
      p.setProperty("id", "4432dd20-fcae-11e3-a3ac-0800200c9a67");

      ISaikuDatasource ds = new Mondrian4Datasource("earthquakes", p);

      try {
        dsm.addDatasource(ds);
      } catch (Exception e) {
        logger.error("Can't add data source to repo", e);
      }
    }
  }

  private static String readFile(String path, Charset encoding) throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, encoding);
  }

  private static String getRepositoryDir() {
    return System.getenv("REPOSITORY_DIR");
  }

  private static String getPath(String resourceName) throws IOException {
    try {
      String repositoryDir = getRepositoryDir();
      if (repositoryDir != null) {
        return new java.io.File(repositoryDir, resourceName).getAbsolutePath();
      }
    } catch (SecurityException ex) {
      logger.error("The installed security manager does not allow you to access the environment variables", ex);
    }

    return new ClassPathResource("data/" + resourceName).getFile().getAbsolutePath();
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    init();
  }
}