package bi.meteorite.saiku.server.mondrian4.connections;

import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.exceptions.SaikuOlapException;
import com.google.common.base.Optional;
import org.olap4j.OlapConnection;
import org.springframework.stereotype.Component;

import java.util.Map;

public interface IConnectionManager {


    void init() throws SaikuOlapException;

    void setDataSourceManager(IDatasourceManager ds);

    IDatasourceManager getDataSourceManager();

    void refreshConnection(String name) throws SaikuOlapException;

    void refreshAllConnections();

    OlapConnection getOlapConnection(String name) throws SaikuOlapException;

    Map<String, OlapConnection> getAllOlapConnections() throws SaikuOlapException;

    Optional<ISaikuConnection> getConnection(String name) throws SaikuOlapException;

    Map<String, ISaikuConnection> getAllConnections() throws SaikuOlapException;

}