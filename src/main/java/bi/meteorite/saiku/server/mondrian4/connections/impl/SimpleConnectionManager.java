package bi.meteorite.saiku.server.mondrian4.connections.impl;

import bi.meteorite.saiku.server.connections.SaikuConnectionFactory;
import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.exceptions.SaikuOlapException;
import bi.meteorite.saiku.server.mondrian4.connections.ISaikuConnection;
import com.google.common.base.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class SimpleConnectionManager extends AbstractConnectionManager {
    private static final Logger log = LoggerFactory.getLogger(SimpleConnectionManager.class);
    private Map<String, ISaikuConnection> connections = new HashMap<>();

    @Override
    @PostConstruct
    public void init() throws SaikuOlapException {
        this.connections.putAll(this.getAllConnections());
    }

    @Override
    protected Optional<ISaikuConnection> getInternalConnection(String name, Optional<ISaikuDatasource> datasource) throws SaikuOlapException {
        Optional<ISaikuConnection> con;

        if (!this.connections.containsKey(name)) {
            con = connect(datasource);

            if (con.isPresent()){
                connections.put(name, con.get());
            }
        } else{
            con = Optional.of(connections.get(name));
        }

        return con;
    }

    @Override
    protected Optional<ISaikuConnection> refreshInternalConnection(String name, Optional<ISaikuDatasource> datasource) {
        ISaikuConnection con = connections.remove(name);

        try {
            con.clearCache();

            return getInternalConnection(name, datasource);
        } catch (Exception e) {
            log.error("Could not get refresh internal connection");
        }

        return Optional.absent();
    }

    private Optional<ISaikuConnection> connect(Optional<ISaikuDatasource> datasource) throws SaikuOlapException{
        ISaikuConnection c;

        try {
            c = SaikuConnectionFactory.getConnection(datasource);
        } catch (Exception e) {
            throw new SaikuOlapException("Connection error occurred", e);
        }

        if(c.initialized()){
            return Optional.of(c);
        } else{
            return Optional.absent();
        }
    }
}
