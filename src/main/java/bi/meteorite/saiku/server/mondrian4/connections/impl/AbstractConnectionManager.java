package bi.meteorite.saiku.server.mondrian4.connections.impl;

import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.exceptions.SaikuOlapException;
import bi.meteorite.saiku.server.mondrian4.connections.IConnectionManager;
import bi.meteorite.saiku.server.mondrian4.connections.ISaikuConnection;
import bi.meteorite.saiku.server.session.UserService;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.olap4j.OlapConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public abstract class AbstractConnectionManager implements IConnectionManager, Serializable {

    @Autowired
    private IDatasourceManager datasourceManager;
    private static final Logger log = LoggerFactory.getLogger(AbstractConnectionManager.class);
    private UserService userService = new UserService();

    public abstract void init() throws SaikuOlapException;

    public void setDataSourceManager(IDatasourceManager ds) {
        this.datasourceManager = ds;
    }

    public IDatasourceManager getDataSourceManager() {
        return datasourceManager;
    }

    public void destroy() throws SaikuOlapException {
        Map<String, OlapConnection> conns = getAllOlapConnections();
        for (Map.Entry<String, OlapConnection> entry : conns.entrySet()) {
            OlapConnection c = entry.getValue();
            try {
                if (!c.isClosed()) {
                    c.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("Can't close connection", e);
            }
        }


    }

    private Optional<ISaikuDatasource> preProcess(Optional<ISaikuDatasource> datasource) {
        //TODO if datasource containers preprocessors then add them
        return datasource;
    }


    private Optional<ISaikuConnection> postProcess(Optional<ISaikuDatasource> datasource, Optional<ISaikuConnection> con) {
        //TODO if datasource containers postprocessors then add them
        return con;
    }

    public Optional<ISaikuConnection> getConnection(String name) throws SaikuOlapException {
        Optional<ISaikuDatasource> data = preProcess(this.datasourceManager.getDatasource(name, false));
        return postProcess(data, getInternalConnection(name, data));
    }

    protected abstract Optional<ISaikuConnection> getInternalConnection(String name, Optional<ISaikuDatasource> datasource)
            throws SaikuOlapException;

    protected abstract Optional<ISaikuConnection> refreshInternalConnection(String name, Optional<ISaikuDatasource> datasource);

    public void refreshAllConnections() {
        this.datasourceManager.load();

        String[] roles = new String[]{};

        try {
            roles = userService.getCurrentUserRoles();
        } catch (Exception ex) {
            // It is expected in scenarios where the user roles are not available
        }


        if (this.datasourceManager.getDatasources(roles).size()>0) {
            for (String name : this.datasourceManager.getDatasources(roles).keySet()) {
                refreshConnection(name);

            }
        }

    }

    public void refreshConnection(String name) {
        Optional<ISaikuDatasource> datasource = preProcess(this.datasourceManager.getDatasource(name));
        Optional<ISaikuConnection> con = refreshInternalConnection(name, datasource);
        postProcess(datasource, con);
    }

    public ImmutableMap<String, ISaikuConnection> getAllConnections() throws SaikuOlapException {
        Map<String, ISaikuConnection> resultDs = new HashMap<>();

        String[] userRoles = userService.getCurrentUserRoles();

        if (this.datasourceManager.getDatasources(userRoles).size() > 0) {
            for (String name : this.datasourceManager.getDatasources(userRoles).keySet()) {
                Optional<ISaikuConnection> con = getConnection(name);

                if (con.isPresent()) {
                    resultDs.put(name, con.get());
                }
            }
        }

        return ImmutableMap.copyOf(resultDs);
    }

    public OlapConnection getOlapConnection(String name) throws SaikuOlapException {
        //TODO return specific connection if user has acceess
        Optional<ISaikuConnection> c = getConnection(name);

        if (c.isPresent()) {
            return (OlapConnection) c.get().getConnection();
        } else {
            throw new SaikuOlapException("Unable to create connection");
        }

    }


    public ImmutableMap<String, OlapConnection> getAllOlapConnections() throws SaikuOlapException {
        Map<String, OlapConnection> olapconnections = new HashMap<>();
        for (ISaikuConnection c : getAllConnections().values()) {
            olapconnections.put(c.getName(), (OlapConnection) c.getConnection());
        }
        return ImmutableMap.copyOf(olapconnections);
    }

    protected boolean isDatasourceSecurity(ISaikuDatasource datasource, String value) {
        if (isDatasourceSecurityEnabled(datasource)) {
            if (datasource.getProperties().containsKey(ISaikuConnection.SECURITY_TYPE_KEY)) {
                return datasource.getProperties().getProperty(ISaikuConnection.SECURITY_TYPE_KEY).equals(value);
            }
        }
        return false;
    }

    private boolean isDatasourceSecurityEnabled(ISaikuDatasource datasource) {
        Properties p = datasource.getProperties();
        if (p.containsKey(ISaikuConnection.SECURITY_ENABLED_KEY)) {
            return Boolean.parseBoolean(p.getProperty(ISaikuConnection.SECURITY_ENABLED_KEY, "false"));
        }
        return false;
    }

}
