package bi.meteorite.saiku.server.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SaikuAPI {
  private static final Logger logger = LoggerFactory.getLogger(SaikuAPI.class);

  @GetMapping("/hello")
  @ResponseBody
  public String helloWorld() {
    return "Hello World";
  }
}
