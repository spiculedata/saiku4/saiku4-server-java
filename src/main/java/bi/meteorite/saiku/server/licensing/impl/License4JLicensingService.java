package bi.meteorite.saiku.server.licensing.impl;

import bi.meteorite.saiku.server.licensing.LicensingService;
import com.license4j.ActivationStatus;
import com.license4j.License;
import com.license4j.LicenseValidator;
import com.license4j.ValidationStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class License4JLicensingService implements LicensingService {


  private Map<String, License> licenses = new HashMap<>();
  private Map<String, License> activatedLicenses = new HashMap<>();


}
