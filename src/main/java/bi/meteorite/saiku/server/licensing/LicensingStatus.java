package bi.meteorite.saiku.server.licensing;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LicensingStatus {
  private String key;
  private boolean isValid;
  private boolean isActivated;
  private String activationString;
  private String name;
  private String company;
  private String type;
  private Integer numberOfUsers;
  private String expiration;
}
