package bi.meteorite.saiku.server.licensing;

public class LicenseException extends Exception {
  public LicenseException() {
    super();
  }

  public LicenseException(String message) {
    super(message);
  }
}
