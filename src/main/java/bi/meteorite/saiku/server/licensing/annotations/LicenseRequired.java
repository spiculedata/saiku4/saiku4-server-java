package bi.meteorite.saiku.server.licensing.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation should be added to methods that are only to be executed by users with valid/active licenses
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LicenseRequired {
  boolean itShouldBeActivated() default true;
}
