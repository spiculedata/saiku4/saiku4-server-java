package bi.meteorite.saiku.server.licensing;

import lombok.Data;

import java.io.Serializable;

@Data
public class SaikuLicense4J2 implements Serializable {
  private String key;
  private String company;
  private String name;
  private String operation;
}
