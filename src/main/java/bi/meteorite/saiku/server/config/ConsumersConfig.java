package bi.meteorite.saiku.server.config;

import bi.meteorite.saiku.server.rpc.producer.LicensingProducer;
import org.saiku.repository.ScopedRepo;
import bi.meteorite.saiku.server.rpc.Protocol;
import bi.meteorite.saiku.server.rpc.consumers.*;
import org.saiku.service.util.security.authorisation.MustBeAuthenticatedAuthorisation;
import org.saiku.web.service.SessionService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConsumersConfig implements InitializingBean {
  @Autowired
  Protocol rpcProtocol;
  @Autowired
  private EmailAndPasswordAuthConsumer emailAndPasswordAuthConsumer;
  @Autowired
  private CookieAuthConsumer cookieAuthConsumer;
  @Autowired
  private CasHeaderAuthConsumer casHeaderAuthConsumer;
  @Autowired
  private GenerateReportConsumer generateReportConsumer;
  @Autowired
  private CreateDatasourceConsumer createDatasourceConsumer;
  @Autowired
  private ListDatasourcesConsumer listDatasourcesConsumer;
  @Autowired
  private EditDatasourceConsumer editDatasourceConsumer;
  @Autowired
  private DeleteDatasourceConsumer deleteDatasourceConsumer;
  @Autowired
  private DataSourceConsumer dataSourceConsumer;
  @Autowired
  private DiscoverDatasourcesConsumer discoverDatasourcesConsumer;
  @Autowired
  private DiscoverCubeMetadataConsumer discoverCubeMetadataConsumer;
  @Autowired
  private LicensingProducer dsp;
  @Autowired
  private ExecuteQueryConsumer executeQueryConsumer;
  @Autowired
  private QueryEndpointsConsumer queryEndpointsConsumer;
  @Autowired
  private ExportQueryConsumer exportQueryConsumer;
  @Autowired
  private ExportImageConsumer exportImageConsumer;

  @Override
  public void afterPropertiesSet() throws Exception {
    rpcProtocol.registerMessage("email_password_auth",  emailAndPasswordAuthConsumer);
    rpcProtocol.registerMessage("cookie_auth",          cookieAuthConsumer);
    rpcProtocol.registerMessage("cas_header_auth",      casHeaderAuthConsumer);
    rpcProtocol.registerMessage("generate_report",      generateReportConsumer);
    rpcProtocol.registerMessage("create_datasource",    createDatasourceConsumer);
    rpcProtocol.registerMessage("list_datasources",     listDatasourcesConsumer);
    rpcProtocol.registerMessage("discover_datasources", discoverDatasourcesConsumer);
    rpcProtocol.registerMessage("get_cube_metadata",    discoverCubeMetadataConsumer);
    rpcProtocol.registerMessage("edit_datasource",      editDatasourceConsumer);
    rpcProtocol.registerMessage("delete_datasource",    deleteDatasourceConsumer);
    rpcProtocol.registerMessage("datasource",           dataSourceConsumer);
    rpcProtocol.registerMessage("execute_query",        executeQueryConsumer);
    rpcProtocol.registerMessage("query_endpoints",      queryEndpointsConsumer);
    rpcProtocol.registerMessage("export_query",         exportQueryConsumer);
    rpcProtocol.registerMessage("export_image",         exportImageConsumer);

    System.out.println(" [x] Saiku Ready - Awaiting RPC requests");

    rpcProtocol.buildAndStart();
  }

  private void initSessionService() throws Exception {
    ScopedRepo sessionRepo = new ScopedRepo();
    MustBeAuthenticatedAuthorisation authorisationPredicate = new MustBeAuthenticatedAuthorisation();

    // bi.meteorite.LicenseUtils licenseUtils = new bi.meteorite.LicenseUtils();
    //licenseUtils.init();

    SessionService sessionService = new SessionService();
    sessionService.setSessionRepo(sessionRepo);
    sessionService.setAuthorisationPredicate(authorisationPredicate);
  }
}
