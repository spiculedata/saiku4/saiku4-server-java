package bi.meteorite.saiku.server.exceptions;

public class SaikuOlapException extends Exception {

    private static final long serialVersionUID = 6079334291828346380L;

    /**
     * @see java.lang.Exception#Exception()
     */
    public SaikuOlapException() {
        super();
    }

    /**
     * @see java.lang.Exception#Exception(String))
     */

    public SaikuOlapException( String message ) {
        super( message );
    }

    /**
     * @see java.lang.Exception#Exception(Throwable)
     */
    public SaikuOlapException( Throwable cause ) {
        super( cause );
    }

    /**
     * @see java.lang.Exception#Exception(String, Throwable)
     */
    public SaikuOlapException( String message, Throwable cause ) {
        super( message, cause );
    }
}
