package bi.meteorite.saiku.server.exceptions;

public class RepositoryException extends Exception {
    private static final long serialVersionUID = 6079334291828346380L;

    /**
     * @see java.lang.Exception#Exception()
     */
    public RepositoryException() {
        super();
    }

    /**
     * @see java.lang.Exception#Exception(String))
     */

    public RepositoryException( String message ) {
        super( message );
    }

    /**
     * @see java.lang.Exception#Exception(Throwable)
     */
    public RepositoryException( Throwable cause ) {
        super( cause );
    }

    /**
     * @see java.lang.Exception#Exception(String, Throwable)
     */
    public RepositoryException( String message, Throwable cause ) {
        super( message, cause );
    }
}
