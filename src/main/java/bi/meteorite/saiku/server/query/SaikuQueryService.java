package bi.meteorite.saiku.server.query;

import bi.meteorite.saiku.server.mondrian4.connections.IConnectionManager;
import lombok.AllArgsConstructor;
import mondrian.rolap.RolapConnection;
import org.apache.commons.lang3.StringUtils;
import org.olap4j.CellSet;
import org.olap4j.OlapConnection;
import org.olap4j.OlapStatement;
import org.olap4j.metadata.Catalog;
import org.olap4j.metadata.Cube;
import org.olap4j.metadata.Database;
import org.olap4j.metadata.Schema;
import org.saiku.olap.dto.SaikuCube;
import org.saiku.olap.dto.resultset.CellDataSet;
import org.saiku.olap.query2.*;
import org.saiku.olap.query2.util.Fat;
import org.saiku.olap.query2.util.Thin;
import org.saiku.olap.util.OlapResultSetUtil;
import org.saiku.olap.util.SaikuProperties;
import org.saiku.olap.util.exception.SaikuOlapException;
import org.saiku.olap.util.formatter.CellSetFormatterFactory;
import org.saiku.olap.util.formatter.ICellSetFormatter;
import org.saiku.query.Query;
import org.saiku.query.util.QueryUtil;
import org.saiku.service.util.QueryContext;
import org.saiku.service.util.exception.SaikuServiceException;
import org.saiku.service.util.export.CsvExporter;
import org.saiku.service.util.export.ExcelExporter;
import org.saiku.web.export.PdfReport;
import org.saiku.web.rest.objects.resultset.QueryResult;
import org.saiku.web.rest.util.RestUtil;
import org.saiku.web.svg.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;

@AllArgsConstructor
@Component
public class SaikuQueryService {
  private static final Logger logger = LoggerFactory.getLogger(SaikuQueryService.class);

  private IConnectionManager connectionManager;

  private final Map<String, QueryContext> context = new HashMap<>();
  private final CellSetFormatterFactory cellSetFormatterFactory = new CellSetFormatterFactory();

  public QueryResult execute(ThinQuery query) throws Exception {
    OlapConnection connection = connectionManager.getOlapConnection(query.getCube().getConnection());

    if (StringUtils.isNotBlank(query.getCube().getCatalog())) {
      connection.setCatalog(query.getCube().getCatalog());
    }

    OlapStatement stmt = connection.createStatement();

    query = updateQuery(query);
    saveQueryInLocalCache(query);

    // Fetch the context for this query
    QueryContext qc = this.context.get(query.getName());

    String mdx = query.getParameterResolvedMdx();
    logger.info("Running query:" + query.getName() + "\tType:" + query.getType() + ":\n" + mdx);

    // Execute the query
    CellSet cellSet = stmt.executeOlapQuery(mdx);
    qc.store(QueryContext.ObjectKey.RESULT, cellSet); // store the result into query's context

    // Format the result
    String formatterName = "";

    if (query.getProperties().containsKey("saiku.olap.result.formatter")) {
      formatterName = query.getProperties().get("saiku.olap.result.formatter").toString();
    }

    ICellSetFormatter formatter = cellSetFormatterFactory.forName(formatterName);

    // Build the query result
    CellDataSet dataSet = OlapResultSetUtil.cellSet2Matrix(cellSet, formatter);
    QueryResult queryResult = RestUtil.convert(dataSet);
    queryResult.setQuery(query);

    return queryResult;
  }

  private ThinQuery saveQueryInLocalCache(ThinQuery tq) {
    if (StringUtils.isBlank(tq.getName())) {
      tq.setName(UUID.randomUUID().toString());
    }

    if (!context.containsKey(tq.getName())) {
      Map<String, Object> cubeProperties = getProperties(tq.getCube());
      tq.getProperties().putAll(cubeProperties);

      QueryContext qt = new QueryContext(QueryContext.Type.OLAP, tq);
      qt.store(QueryContext.ObjectKey.QUERY, tq);
      this.context.put(tq.getName(), qt);
    }

    return tq;
  }

  public ThinQuery updateQuery(ThinQuery old) throws Exception {
    if (ThinQuery.Type.QUERYMODEL.equals(old.getType())) {
      Cube cub = getNativeCube(old.getCube());
      Query q = Fat.convert(old, cub);
      List<ThinCalculatedMember> cms = old.getQueryModel().getCalculatedMembers();
      ThinQuery tqAfter = Thin.convert(q, old.getCube());
      tqAfter.getQueryModel().setCalculatedMembers(cms);
      getEnabledCMembers(old.getQueryModel(), tqAfter.getQueryModel());

      // Set measures aggregators
      for (ThinMeasure measure : tqAfter.getQueryModel().getDetails().getMeasures()) {
        for (ThinMeasure oldMeasure : old.getQueryModel().getDetails().getMeasures()) {
          if (measure.getUniqueName().equals(oldMeasure.getUniqueName())) {
            measure.getAggregators().addAll(oldMeasure.getAggregators());
            break;
          }
        }
      }

      old.setQueryModel(tqAfter.getQueryModel());
      old.setMdx(tqAfter.getMdx());
    }

    String mdx = old.getMdx();
    List<String> params = QueryUtil.parseParameters(mdx);
    old.addParameters(params);

    Map<String, Object> cubeProperties = getProperties(old.getCube());
    old.getProperties().putAll(cubeProperties);

    old = removeDupSelections(old);
    return old;
  }

  private ThinQuery removeDupSelections(ThinQuery old) {
    Map<ThinQueryModel.AxisLocation, ThinAxis> map = old.getQueryModel().getAxes();
    for (Map.Entry<ThinQueryModel.AxisLocation, ThinAxis> entry : map.entrySet()) {
      for (ThinHierarchy h : entry.getValue().getHierarchies()) {
        Map<String, ThinLevel> map2 = h.getLevels();
        for (Map.Entry<String, ThinLevel> levelentry : map2.entrySet()) {
          List<ThinMember> members = levelentry.getValue().getSelection().getMembers();

          List<ThinMember> uniqueMembers = new ArrayList<>();
          Map<String, ThinMember> temp = new HashMap<>();
          for (ThinMember tm : members) {
            temp.put(tm.getUniqueName(), tm);
          }

          for (ThinMember tm : members) {
            if (temp.containsKey(tm.getUniqueName())) {
              uniqueMembers.add(tm);
              temp.remove(tm.getUniqueName());
            }
          }

          levelentry.getValue().getSelection().setMembers(uniqueMembers);
        }
      }
    }
    return old;
  }

  public Map<String, Object> getProperties(SaikuCube cube) {
    Map<String, Object> properties = new HashMap<>();
    try {
      Cube c = getNativeCube(cube);
      OlapConnection con = c.getSchema().getCatalog().getDatabase().getOlapConnection();
      properties.put("saiku.olap.query.drillthrough", c.isDrillThroughEnabled());
      properties.put("org.saiku.query.explain", con.isWrapperFor(RolapConnection.class));

      try {
        Boolean isScenario = (c.getDimensions().get("Scenario") != null);
        properties.put("org.saiku.connection.scenario", isScenario);
      } catch (Exception e) {
        properties.put("org.saiku.connection.scenario", false);
      }
    } catch (Exception e) {
      throw new SaikuServiceException(e);
    }

    return properties;
  }

  private void getEnabledCMembers(ThinQueryModel qm, ThinQueryModel queryModel) {
    int i = 0;
    for (Map.Entry<ThinQueryModel.AxisLocation, ThinAxis> entry : qm.getAxes().entrySet()) {
      ThinAxis v = entry.getValue();
      for (ThinHierarchy h : v.getHierarchies()) {
        for (Map.Entry<String, ThinLevel> entry1 : h.getLevels().entrySet()) {
          ThinLevel v2 = entry1.getValue();
          if (v2.getSelection() != null) {
            for (ThinMember m : v2.getSelection().getMembers()) {

              if (m.getType() != null && m.getType().equals("calculatedmember")) {
                Map<ThinQueryModel.AxisLocation, ThinAxis> ax = queryModel.getAxes();
                ThinAxis sax = ax.get(entry.getKey());
                List<ThinHierarchy> h2 = sax.getHierarchies();
                Map<String, ThinLevel> l = h2.get(i).getLevels();
                ThinLevel l2 = l.get(entry1.getKey());
                l2.getSelection().getMembers().add(m);

                queryModel.getAxes().get(entry.getKey()).getHierarchies().get(i).getLevels().get(entry1
                    .getKey()).getSelection().getMembers().add(m);

              }
            }
          }
        }
        i++;
      }
    }
  }

  public Cube getNativeCube(SaikuCube cube) throws SaikuOlapException {
    try {
      OlapConnection con = connectionManager.getOlapConnection(cube.getConnection());
      if (con != null ) {
        for (Database db : con.getOlapDatabases()) {
          Catalog cat = db.getCatalogs().get(cube.getCatalog());
          if (cat != null) {
            for (Schema schema : cat.getSchemas()) {
              if ((StringUtils.isBlank(cube.getSchema()) && StringUtils.isBlank(schema.getName())) ||
                  schema.getName().equals(cube.getSchema())) {
                for (Cube cub : schema.getCubes()) {
                  if (cub.getName().equals(cube.getName()) || cub.getUniqueName().equals(cube.getName())) {
                    return cub;
                  }
                }
              }
            }
          }
        }
      }
    } catch (Exception e) {
      throw new SaikuOlapException("Cannot get native cube for ( " + cube+ " )",e);
    }

    throw new SaikuOlapException("Cannot get native cube for ( " + cube+ " )");
  }

  public byte[] exportPdf(String queryName, String format) throws Exception {
    CellDataSet cellData = getFormattedResult(queryName, format);
    QueryResult queryResult = RestUtil.convert(cellData);
    PdfReport pdf = new PdfReport();

    return pdf.createPdf(queryResult, format);
  }

  public byte[] exportCsv(String queryName, String format) throws Exception {
    QueryContext qc = this.context.get(queryName);
    CellSet cellSet = qc.getOlapResult();
    String formatterName = (StringUtils.isBlank(format) ? "" : format.toLowerCase());
    ICellSetFormatter formatter = cellSetFormatterFactory.forName(formatterName);

    return CsvExporter.exportCsv(cellSet, SaikuProperties.webExportCsvDelimiter, SaikuProperties.webExportCsvTextEscape, formatter);
  }

  public byte[] exportXls(String queryName, String format) throws Exception {
    QueryContext qc = this.context.get(queryName);
    ThinQuery query = qc.getOlapQuery();
    CellSet cellSet = qc.getOlapResult();
    String formatterName = (StringUtils.isBlank(format) ? "" : format.toLowerCase());
    ICellSetFormatter formatter = cellSetFormatterFactory.forName(formatterName);
    CellDataSet table = OlapResultSetUtil.cellSet2Matrix(cellSet, formatter);

    List<ThinHierarchy> filterHierarchies = null;

    if (ThinQuery.Type.QUERYMODEL.equals(query.getType())) {
      filterHierarchies = query.getQueryModel().getAxes().get(ThinQueryModel.AxisLocation.FILTER).getHierarchies();
    }

    return ExcelExporter.exportExcel(table, formatter, filterHierarchies);
  }

  private CellDataSet getFormattedResult(String queryName, String format) {
    QueryContext qc = this.context.get(queryName);
    CellSet cellSet = qc.getOlapResult();
    String formatterName = (StringUtils.isBlank(format) ? "" : format.toLowerCase());
    ICellSetFormatter formatter = cellSetFormatterFactory.forName(formatterName);

    return OlapResultSetUtil.cellSet2Matrix(cellSet, formatter);
  }

  public byte[] exportImage(String type, String svg, Integer size) throws Exception {
    InputStream in = new ByteArrayInputStream(svg.getBytes("UTF-8"));
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    Converter converter = Converter.byType(type.toUpperCase());

    if (converter == null) {
      throw new Exception("Missing converter.");
    }

    converter.convert(in, out, size);
    out.flush();

    return out.toByteArray();
  }
}
