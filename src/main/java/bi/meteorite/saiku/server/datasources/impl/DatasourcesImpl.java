package bi.meteorite.saiku.server.datasources.impl;

import bi.meteorite.saiku.server.datasources.Datasources;
import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DatasourcesImpl implements Datasources {


    private ImmutableMap<String, Object> sources;
    private Map<String, ISaikuDatasource> loadedDataSources = new HashMap<>();

    @Autowired
    public DatasourcesImpl(){

    }

    @Override
    public Optional<ImmutableMap<String, Object>> getDataSources() {
        return Optional.of(sources);
    }

    @Override
    public void setDataSources(Map<String, Object> datasources) {

        sources = ImmutableMap.copyOf(datasources);
    }

    @Override
    public void setLoadedDataSources(ImmutableMap<String, ISaikuDatasource> loadedDataSources) {

    }

    @Override
    public ImmutableMap<String, ISaikuDatasource> getLoadedDataSources() {
        return ImmutableMap.copyOf(loadedDataSources);
    }

    @Override
    public void addLoadedDataSource(ISaikuDatasource ds) {
        this.loadedDataSources.put(ds.getName(), ds);
    }
}
