package bi.meteorite.saiku.server.datasources.impl;

import bi.meteorite.saiku.server.datasources.ISaikuDatasource;

import java.util.Properties;

public class Mondrian4Datasource implements ISaikuDatasource {

    private String name;
    private Properties props;

    public Mondrian4Datasource(){

    }

    public Mondrian4Datasource(String name, Properties properties){
        this.name = name;
        this.props = properties;
    }

    @Override
    public String getType() {
        return "OLAP";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Properties getProperties() {
        return props;
    }

    @Override
    public void setProperties(Properties props) {
        this.props = props;
    }

    @Override
    public ISaikuDatasource clone() {
        return new Mondrian4Datasource( name, (Properties) props.clone() );
    }
}
