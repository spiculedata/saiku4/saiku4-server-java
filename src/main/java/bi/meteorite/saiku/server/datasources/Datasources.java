package bi.meteorite.saiku.server.datasources;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface Datasources {

    Optional<ImmutableMap<String, Object>> getDataSources();
    void setDataSources(Map<String, Object> dataSources);

    void setLoadedDataSources(ImmutableMap<String, ISaikuDatasource> loadedDataSources);
    ImmutableMap<String, ISaikuDatasource> getLoadedDataSources();

    void addLoadedDataSource(ISaikuDatasource ds);
}
