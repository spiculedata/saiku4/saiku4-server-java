package bi.meteorite.saiku.server.datasources;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.olap4j.OlapConnection;

import java.util.List;

public interface IDatasourceManager {
    void load();

    void addDatasource(ISaikuDatasource datasource) throws Exception;

    ImmutableMap<String, ISaikuDatasource> addDatasources(List<ISaikuDatasource> datasources);

    boolean removeDatasource(String datasourceName);

    ImmutableMap<String, ISaikuDatasource> getDatasources(String[] roles);

    Optional<ISaikuDatasource> getDatasource(String datasourceName);

    Optional<ISaikuDatasource> getDatasource(String datasourceName, boolean refresh);

    void createUser(String user);

    void addSchema(String file, String path, String name) throws Exception;

    List<org.saiku.olap.dto.SaikuConnection> getSaikuConnections() throws Exception;

    OlapConnection getOlapConnection(String name) throws Exception;
}