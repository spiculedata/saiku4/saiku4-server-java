package bi.meteorite.saiku.server.datasources.impl;

import bi.meteorite.saiku.server.connections.SaikuConnectionFactory;
import bi.meteorite.saiku.server.datasources.Datasources;
import bi.meteorite.saiku.server.datasources.IDatasourceManager;
import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.mondrian4.connections.ISaikuConnection;
import bi.meteorite.saiku.server.mondrian4.connections.impl.MondrianOlapConnection;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.olap4j.OlapConnection;
import org.olap4j.OlapDatabaseMetaData;
import org.olap4j.OlapException;
import org.olap4j.metadata.Catalog;
import org.olap4j.metadata.Cube;
import org.olap4j.metadata.Schema;
import org.saiku.olap.dto.SaikuCatalog;
import org.saiku.olap.dto.SaikuConnection;
import org.saiku.olap.dto.SaikuCube;
import org.saiku.olap.dto.SaikuSchema;
import org.saiku.olap.util.SaikuCubeCaptionComparator;
import org.saiku.olap.util.exception.SaikuOlapException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Component
public class Saiku4DatasourceManager  implements IDatasourceManager {
    private static final Logger logger = LoggerFactory.getLogger(Saiku4DatasourceManager.class);

    @Autowired
    private Datasources datasources;

    private Map<String, OlapConnection> olapConnections = new HashMap<>();

    @Override
    public void load() {
        //TODO make dynamic to load all datasource types
        //Probably makes sense to do that when we split backends into individual components and just load datasources for their specific type
        /**
         * {
         *      datasources: [
         *             {
         *               "name": "test",
         *               "driver": "mondrian.olap4j.MondrianOlap4jDriver",
         *               "location": "jdbc:mondrian:Jdbc=jdbc:mysql://localhost/test;Catalog=file://tmp/myschema.xml;JdbcDrivers=com.mysql.jdbc.Driver;",
         *               "username": "bugg2",
         *               "password": "fdr6v5",
         *               "schema": "<xml></xml>",
         *               "securityenabled": false,
         *               "securitytype": null,
         *               "securitymapping": null
         *             }
         *       ]
         * }
         */
        Optional<ImmutableMap<String, Object>> dsources = datasources.getDataSources();
        if(dsources.isPresent()) {
            for (Map.Entry<String, Object> entry : dsources.get().entrySet()) {
                ArrayList c = (ArrayList) entry.getValue();
                System.out.println(c);
                for(Object l : c){
                    HashMap m = (HashMap) l;
                    Properties props = new Properties();
                    props.putAll(m);
                    ISaikuDatasource ds = new Mondrian4Datasource((String) m.get("name"), props);
                    datasources.addLoadedDataSource(ds);
                }

            }

        }
    }

    @Override
    public void addDatasource(ISaikuDatasource datasource) throws Exception {
        datasources.addLoadedDataSource(datasource);
    }

    @Override
    public ImmutableMap<String, ISaikuDatasource> addDatasources(List<ISaikuDatasource> datasources) {
        for(ISaikuDatasource ds : datasources){
            this.datasources.addLoadedDataSource(ds);
        }
        return this.datasources.getLoadedDataSources();
    }

    @Override
    public boolean removeDatasource(String datasourceName) {
        return false;
    }


    @Override
    public ImmutableMap<String, ISaikuDatasource> getDatasources(String[] roles) {
        return this.datasources.getLoadedDataSources();
    }

    @Override
    public Optional<ISaikuDatasource> getDatasource(String datasourceName) {
        if(this.datasources.getLoadedDataSources().size()>0){
            ISaikuDatasource ds = this.datasources.getLoadedDataSources().get(datasourceName);
            return Optional.of(ds);
        }
        return Optional.absent();
    }

    @Override
    public Optional<ISaikuDatasource> getDatasource(String datasourceName, boolean refresh) {
        //TODO needs to refresh cache
        return getDatasource(datasourceName);
    }

    @Override
    public void createUser(String user) {
        // TODO - create user dir
    }

    @Override
    public void addSchema(String file, String path, String name) throws Exception {
        // TODO - save schema file
    }

    @Override
    public List<SaikuConnection> getSaikuConnections() throws Exception {
        List<SaikuConnection> oldSaikuConnections = new ArrayList<>();
        ImmutableMap<String, ISaikuDatasource> datasources = getDatasources(new String[] {});

        for (ISaikuDatasource ds : datasources.values()) {
            ISaikuConnection con = SaikuConnectionFactory.getConnection(Optional.of(ds));

            if (con != null) {
                MondrianOlapConnection mondrianCon = (MondrianOlapConnection)con;
                oldSaikuConnections.add(createOldSaikuConnection(con.getName(), (OlapConnection)mondrianCon.getConnection()));
            }
        }

        Collections.sort(oldSaikuConnections);

        return oldSaikuConnections;
    }

    @Override
    public OlapConnection getOlapConnection(String name) throws Exception {
        return olapConnections.get(name);
    }

    private org.saiku.olap.dto.SaikuConnection createOldSaikuConnection(String connectionName, OlapConnection olapcon) throws SaikuOlapException {
        olapConnections.put(connectionName, olapcon);

        org.saiku.olap.dto.SaikuConnection connection;

        List<SaikuCatalog> catalogs = new ArrayList<>();

        try {
            for (Catalog cat : olapcon.getOlapCatalogs()) {
                List<SaikuSchema> schemas = new ArrayList<>();

                for (Schema schem : cat.getSchemas()) {
                    List<SaikuCube> cubes = new ArrayList<>();

                    for (Cube cub : schem.getCubes()) {
                        cubes.add(new SaikuCube(connectionName, cub.getUniqueName(), cub.getName(), cub.getCaption(), cat.getName(), schem.getName(), cub.isVisible()));
                    }

                    Collections.sort(cubes, new SaikuCubeCaptionComparator());

                    schemas.add(new SaikuSchema(schem.getName(),cubes));
                }

                if (schemas.size() == 0) {
                    OlapDatabaseMetaData olapDbMeta = olapcon.getMetaData();
                    ResultSet cubesResult = olapDbMeta.getCubes(cat.getName(), null, null);

                    try {
                        List<SaikuCube> cubes = new ArrayList<>();
                        while(cubesResult.next()) {
                            cubes.add(new SaikuCube(connectionName, cubesResult.getString("CUBE_NAME"),cubesResult.getString("CUBE_NAME"),
                                cubesResult.getString("CUBE_NAME"), cubesResult.getString("CATALOG_NAME"),cubesResult.getString("SCHEMA_NAME")));
                        }

                        Collections.sort(cubes, new SaikuCubeCaptionComparator());

                        schemas.add(new SaikuSchema("",cubes));
                    } catch (SQLException e) {
                        throw new OlapException(e.getMessage(),e);
                    } finally {
                        try {
                            cubesResult.close();
                        } catch (SQLException e) {
                            logger.error("Could not close cubesResult", e.getNextException());
                        }
                    }
                }
                Collections.sort(schemas);
                catalogs.add(new SaikuCatalog(cat.getName(),schemas));
            }
        } catch (OlapException e) {
            throw new SaikuOlapException("Error getting objects of connection (" + connectionName + ")" ,e);
        }

        Collections.sort(catalogs);

        connection = new SaikuConnection(connectionName, catalogs);

        return connection;
    }
}
