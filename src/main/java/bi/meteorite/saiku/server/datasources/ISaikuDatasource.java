package bi.meteorite.saiku.server.datasources;

import java.util.Properties;

public interface ISaikuDatasource {

    public String getType();

    String getName();
    void setName(String name);

    Properties getProperties();
    void setProperties(Properties props);

    ISaikuDatasource clone();

}
