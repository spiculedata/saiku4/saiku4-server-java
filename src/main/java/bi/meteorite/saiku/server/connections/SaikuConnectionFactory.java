package bi.meteorite.saiku.server.connections;


import bi.meteorite.saiku.server.datasources.ISaikuDatasource;
import bi.meteorite.saiku.server.mondrian4.connections.ISaikuConnection;
import bi.meteorite.saiku.server.mondrian4.connections.impl.MondrianOlapConnection;
import com.google.common.base.Optional;

public class SaikuConnectionFactory {


  public static ISaikuConnection getConnection(Optional<ISaikuDatasource> datasource ) throws Exception {
    if(datasource.isPresent()) {
      if ("OLAP".equals(datasource.get().getType())) {
        ISaikuConnection con = new MondrianOlapConnection(datasource.get().getName(), datasource.get().getProperties());
        if (con.connect()) {
          return con;
        }
      }
    }
    return null;
  }
}