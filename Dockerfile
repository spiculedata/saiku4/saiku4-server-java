FROM debian:stretch

WORKDIR /saiku-backend

COPY target/ /saiku-backend
COPY /data /data
